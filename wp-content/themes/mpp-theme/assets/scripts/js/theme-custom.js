(function( root, $, undefined ) {

    $(function () {

        var mySwiper = new Swiper('.swiper-container', {
            direction: 'horizontal',
            slidesPerView: 2,
            loop: true,
            spaceBetween: 40,
            speed:1300,
            autoplay:{
                delay:3000,
                disableOnInteraction:false
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 50
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 50
                },
                1200: {
                    slidesPerView: 6,
                    spaceBetween: 50
                },
                1440: {
                    slidesPerView: 7,
                    spaceBetween: 70
                }
            }
        });

        var rellax = new Rellax('.rellax');

        var scene = document.getElementById('scene-0'),
        scene1 = document.getElementById('scene-1');
        var parallaxInstance = new Parallax(scene);
        var parallaxInstance1 = new Parallax(scene1);

        $('.close-button').click(function(){
            setTimeout(function(){
                $('.off-canvas #offcanvas-nav').foundation('up', $('.is-accordion-submenu'));
            }, 700);
        })

    });

} ( this, jQuery ));
