<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>
<?php
$image = get_field('footer_logo','option');
$footer_heading = get_field('footer_text_heading','option');
$footer_paragraph = get_field('footer_text_paragraph','option');
?>


				<footer class="footer" role="contentinfo">

					<div class="grid-container">

                        <div class="inner-footer">

                            <div class="inner-footer__left">

                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />

                            </div>

                            <div class="inner-footer__right">

                                <h4><?php echo $footer_heading; ?></h4>

                                <p><?php echo $footer_paragraph; ?></p>

                            </div>

                        </div>

					</div> <!-- end #inner-footer -->

				</footer> <!-- end .footer -->

			</div>  <!-- end .off-canvas-content -->

		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
