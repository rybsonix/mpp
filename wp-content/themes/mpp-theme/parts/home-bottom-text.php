<?php
$bottom_text = get_field('bottom_text');
$bottom_text_author = get_field('bottom_text_author');
?>
<section class="home-bottom-text">
    <div class="bg-images">
        <div>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/chart.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="-2" data-rellax-xs-speed="0" data-rellax-mobile-speed="0">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/flara.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="1" data-rellax-xs-speed="0" data-rellax-mobile-speed="0">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/path312.png" alt="">
        </div>
    </div>
    <div class="grid-container">
        <div class="home-bottom-text__main">
            <p><?php echo $bottom_text; ?></p>
            <p><?php echo $bottom_text_author; ?></p>
        </div>
    </div>
</section>
