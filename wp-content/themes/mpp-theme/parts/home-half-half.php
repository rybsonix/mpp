<?php if( have_rows('home_half-half_section_one_column') ): ?>
    <section class="home-half-half">
        <div class="grid-container">
            <div class="home-half-half__heading">
                <h2>NEWS</h2>
            </div>
            <div class="home-half-half__main">
                <div class="grid-x grid-margin-x">
                    <?php
                    $e = 0;
                    while( have_rows('home_half-half_section_one_column') ): the_row();
                    $column_image = get_sub_field('column_graphic');
                    $column_title = get_sub_field('column_title');
                    $column_description = get_sub_field('column_description');
                    $column_button = get_sub_field('column_button');
                    $column_button_url = $column_button['url'];
                    $column_button_title = $column_button['title'];
                    $column_button_target = $column_button['target'] ? $column_button['target'] : '_self';
                    ?>
                    <div class="cell medium-6">
                        <div class="home-half-half__card">
                            <div class="home-half-half__img">
                                <div id="scene-<?php echo $e; ?>" data-hover-only="true" data-pointer-events="true">
                                    <div data-depth="0.3">
                                        <img src="<?php echo esc_url($column_image['url']); ?>" alt="<?php echo esc_attr($column_image['alt']); ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="home-half-half__text">
                                <h3><?php echo $column_title; ?></h3>
                                <p><?php echo $column_description; ?></p>
                                <a class="button glow-button" href="<?php echo esc_url( $column_button_url ); ?>" target="<?php echo esc_attr( $column_button_target ); ?>"><?php echo esc_html( $column_button_title ); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php
                    $e++;
                    endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
