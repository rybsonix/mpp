<?php
$hero_title = get_field('hero_title');
$hero_desc = get_field('hero_desc');
$hero_button = get_field('hero_button');
$hero_button_url = $hero_button['url'];
$hero_button_title = $hero_button['title'];
$hero_button_target = $hero_button['target'] ? $hero_button['target'] : '_self';
$hero_graphic = get_field('hero_graphic');
?>
<section class="home-hero">
    <div class="bg-images">
        <div class="rellax" data-rellax-speed="1">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/chart.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="-2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/flara.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="3">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/path312.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="-1">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/grid.png" alt="">
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell medium-6">
                <div class="home-hero__text">
                    <h1><?php echo $hero_title ;?></h1>
                    <p><?php echo $hero_desc ;?></p>
                    <a class="button glow-button" href="<?php echo esc_url( $hero_button_url ); ?>" target="<?php echo esc_attr( $hero_button_target ); ?>"><?php echo esc_html( $hero_button_title ); ?></a>
                </div>
            </div>
            <div class="cell medium-6">
                <div class="home-hero__img">
                    <img src="<?php echo esc_url($hero_graphic['url']); ?>" alt="<?php echo esc_attr($hero_graphic['alt']); ?>" />
                </div>
            </div>
        </div>
    </div>
</section>
