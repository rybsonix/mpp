<section class="home-middle">
    <div class="bg-images">
        <div class="rellax" data-rellax-speed="-3">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/flara.png" alt="">
        </div>
    </div>
    <div class="grid-container">
        <div class="home-middle__text">
            <?php the_content(); ?>
        </div>
    </div>
</section>
