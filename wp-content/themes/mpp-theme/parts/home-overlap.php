<?php
$overlap_title = get_field('overlap_title');
$overlap_desc = get_field('overlap_description');
$overlap_img = get_field('overlap_graphic');
?>
<section class="home-overlap">
    <div class="bg-images">
        <div class="rellax" data-rellax-speed="2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/grid-vertical.png" alt="">
        </div>
        <div class="rellax" data-rellax-speed="2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/grid-vertical.png" alt="">
        </div>
    </div>
    <div class="grid-container">
        <div class="home-overlap__container">
            <div class="home-overlap__text">
                <h2><?php echo $overlap_title; ?></h2>
                <p><?php echo $overlap_desc; ?></p>
            </div>
            <div class="rellax" data-rellax-speed="2" data-rellax-xs-speed="0">
                <div class="home-overlap__img">
                    <img src="<?php echo esc_url($overlap_img['url']); ?>" alt="<?php echo esc_attr($overlap_img['alt']); ?>" />
                </div>
            </div>
        </div>
    </div>
</section>
