<?php if( have_rows('home_slider') ): ?>
    <section class="home-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php while( have_rows('home_slider') ): the_row();
                $image = get_sub_field('slider_graphic');
                $desc = get_sub_field('slider_graphic_description');
                $css_class = get_sub_field('slide_css_class');
                ?>
                <div class="swiper-slide <?php echo $css_class; ?>">
                    <div class="swiper-slide__container">
                        <div class="swiper-slide__front">
                            <div class="inner">
                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            </div>
                        </div>
                        <div class="swiper-slide__back">
                            <div class="inner">
                                <p><?php echo $desc; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
