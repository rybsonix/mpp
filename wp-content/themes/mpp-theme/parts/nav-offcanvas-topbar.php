<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>
<?php $image = get_field('main_logo','option'); ?>
<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left">
		<ul class="menu">
			<li><a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></a></li>
		</ul>
	</div>
	<div class="top-bar-right show-for-large">
		<?php joints_top_nav(); ?>
	</div>
	<div class="top-bar-right hide-for-large">
		<div class="ham-menu" data-toggle="off-canvas">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
</div>
